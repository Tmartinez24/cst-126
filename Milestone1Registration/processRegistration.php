<?php
/*
 //Tyler Martinez
 //CST126
 //Milestone1- Registration Form
 // This is the php for accepting the information from the user and then pushing it into the mysql database
 */

$servername = "localhost";
$username = "root";
$password = "root";
$database_name = "milestoneregistrationform";
$userName = $_GET['nameInput'];
$userEmail = $_GET['emailInput'];
$userLogin = $_GET['usernameInput'];
$userPassword = $_GET['passwordInput'];

$connection = mysqli_connect($servername, $username, $password, $database_name); // connects to the database

if(!$connection) // Tests if the connection succeded
{
    die("Connection failed: " . mysqli_connect_error());
}
echo nl2br("Connected successfully\n");

$sql_statement = "INSERT INTO `registeredusers` (`id`, `name`, `email`,`username`,`password`) VALUES(NULL,'$userName','$userEmail','$userLogin','$userPassword')"; //pushes the info into the database

if(mysqli_query($connection,$sql_statement)) // Tests if the information can be pushed to the database
{
    echo nl2br("New record created\nThank you for registering: " . $userName);
}
else
{
    echo "Error: " . $sql_statement . "<br>" . mysqli_error($connection);
}

mysqli_close($connection);
?> 