<?php
/*
 //Tyler Martinez
 //CST126
 //Milestone2- Login Form
 //Version 0.2 - This is the php for checking the database for the username and password. If it finds the user in the database then the user will be successfully logged in.
 // In future updates I would like to add password attempts that would lock out the user after too many failed passwords in a set time.
 */

$servername = "localhost";
$username = "root";
$password = "root";
$database_name = "milestoneregistrationform";
$personUsername = $_POST['inputUsername'];
$personPassword = $_POST['inputPassword'];

if($personUsername == NULL)
{
    echo "Error: The Username is a required field and cannot be blank.";
}
else
{
    if($personPassword == NULL)
    {
        echo "Error: The Password is a required field and cannot be blank.";
    }
    else
    {
        
        $connection = mysqli_connect($servername, $username, $password, $database_name);        
        
        $sql_statement = "SELECT * FROM `registeredusers` WHERE `USERNAME` LIKE '$personUsername' AND `PASSWORD` LIKE '$personPassword'";
        $result = mysqli_query($connection, $sql_statement);
             
        if (mysqli_num_rows($result) == 1)
        {
            echo "Login was Successful";
        }
        else if(mysqli_num_rows($result) == 0)
        {
            echo "Login Failed";
        }
        else
        {
            echo "Database Error";
        }        
    }
}

?>