<?php
$servername = "localhost";
$username = "root";
$password = "root";
$database_name = "activity1";

$connection = mysqli_connect($servername, $username, $password, $database_name);

if(!$connection)
{
    die("Connection failed: " . mysqli_connect_error());
}
echo "Connected successfully <br>";


$sql_statement = "SELECT ID, FIRST_NAME, LAST_NAME FROM `users`";
$result = mysqli_query($connection, $sql_statement);

if (mysqli_num_rows($result) > 0) 
{   
    while($row = mysqli_fetch_assoc($result)) 
    {
        echo "id: " . $row["ID"]. " - Name: " . $row["FIRST_NAME"]. " " . $row["LAST_NAME"]. "<br>";
    }
}
else 
{
    echo "0 results";
}




mysqli_close($connection);

?>